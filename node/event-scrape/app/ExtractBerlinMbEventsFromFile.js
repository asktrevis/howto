const fs = require('fs');
const cheerio = require("cheerio");

// Async function which scrapes the data
function scrapeData(filename) {
    try {
        // Fetch HTML of the page we want to scrape
        var data = fs.readFileSync(filename, 'utf8');
        const $ = cheerio.load(data);
        const events = [];

        const SELECTOR = '#content > div.full.clearfix > div > div.m-events.m-events-full.m-filtered-list';

        let divs = $(SELECTOR).children();
        // console.log($(SELECTOR).html());
        // console.log("number" + divs.length);

        const eventObj = { eventDate: "", eventTitle: "" };

        for (var i = 0; i < divs.length; i++) {
            //tmpTitle = $(divs[i]).html();
            let tmpTitle = $(divs[i]).find("h3").first().text().trim();
            let myDay = $(divs[i]).find('div.date > span > span.m-date__day').first().text().trim();
            let myMonth = $(divs[i]).find('div.date > span > span.m-date__month').first().text().trim();
            let myYear = $(divs[i]).find('div.date > span > span.m-date__year').first().text().trim();
            let tmpDate = (myYear + "-" + myMonth + "-" + myDay).replaceAll(/[\.\,]/ig, '');
            if (tmpTitle.length > 0 && tmpDate.length > 0) {
                console.log(tmpDate + " " + tmpTitle);
                tmpTitle = undefined;
                tmpDate = undefined;
            }
        }

    } catch (err) {
        console.error(err);
    }
}

scrapeData(process.argv[2])

