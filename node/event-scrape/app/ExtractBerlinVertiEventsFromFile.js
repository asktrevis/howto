const fs = require('fs');
const cheerio = require("cheerio");

function decodeMonth(month) {
    switch (removeObsoleteChars(month)) {
        case 'Jan':
            return "01";
            break;
        case 'Feb':
            return "02";
            break;
        case 'Mär':
            return "03";
            break;
        case 'Apr':
            return "04";
            break;
        case 'Mai':
            return "05";
            break;
        case 'Jun':
            return "06";
            break;
        case 'Jul':
            return "07";
            break;
        case 'Aug':
            return "08";
            break;
        case 'Sep':
            return "09";
            break;
        case 'Okt':
            return "10";
            break;
        case 'Nov':
            return "11";
            break;
        case 'Dez':
            return "12";
            break;
        default:
            return null;
    }
}

function parseDate(strDay, strYear, strMonth) {
    return removeObsoleteChars(strYear) + "-" + decodeMonth(strMonth.replace('.','')) + "-" + removeObsoleteChars(strDay.replace('.',''));
}

function removeObsoleteChars(str) {
    return (str) ? str.replace(/(\r\n|\n|\r)/gm, ' ')
        .replace(/\s\s+/g, ' ')
        .trim() : null;
}

// Async function which scrapes the data
function scrapeData(filename) {
    try {
        // Fetch HTML of the page we want to scrape
        var data = fs.readFileSync(filename, 'utf8');
        const $ = cheerio.load(data);
        const events = [];

        const SELECTOR = 'div.m-events.m-events-full.m-filtered-list';

        let divs = $(SELECTOR).children();
        // console.log($(SELECTOR).html());
        // console.log("number" + divs.length);

        const eventObj = {
            eventTitle: "", eventDate: ""
        };

        for (var i = 0; i < divs.length; i++) {
            let element = $(divs[i]);

            //let tmpTitle = $(divs[i]).find("h3").first().text().trim();
            let tmpTitle = $('h3', element).text();
            if (tmpTitle) {
                eventObj.eventTitle = removeObsoleteChars(tmpTitle);
                // console.log("1x" + i + " " + eventObj.eventTitle);
                tmpTitle = undefined;
            }
            
            // let myDay = $(divs[i]).find('div.date > span > span.m-date__day').first().text().trim();
            // let myMonth = $(divs[i]).find('div.date > span > span.m-date__month').first().text().trim();
            // let myYear = $(divs[i]).find('div.date > span > span.m-date__year').first().text().trim();
            
            let tmpDtDay = $('div.date > span > span.m-date__day',element).text();
            let tmpDtYear = $('div.date > span > span.m-date__year',element).text();
            let tmpDtMonth = $('div.date > span > span.m-date__month',element).text();
            //console.log("2x" + i + " " + tmpDtDay+ " " + tmpDtMonth+ " " + tmpDtYear);

            // let tmpDate = (myYear + "-" + myMonth + "-" + myDay).replaceAll(/[\.\,]/ig, '');
            // if (tmpTitle.length > 0 && tmpDate.length > 0) {
            //     console.log(tmpDate + " " + tmpTitle);
            //     tmpTitle = undefined;
            //     tmpDate = undefined;
            // }
            if (tmpDtDay && tmpDtYear && tmpDtMonth) {
                eventObj.eventDate = parseDate(tmpDtDay, tmpDtYear, tmpDtMonth);
                // console.log("2x" + i + " " + eventObj.eventDate);
                tmpDtTime = undefined;
                tmpDtYear = undefined;
                tmpDtMonth = undefined;
            }
            if (eventObj.eventDate && eventObj.eventTitle) {
                console.log(eventObj.eventDate + " " + eventObj.eventTitle);

                delete eventObj.eventDate;
                delete eventObj.eventTitle;
            }

        }

    } catch (err) {
        console.error(err);
    }
}

scrapeData(process.argv[2])

