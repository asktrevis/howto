const axios = require("axios");
const cheerio = require("cheerio");
const fs = require("fs");
const dtfns = require('date-fns');
const pretty = require("pretty");

// URL of the page we want to scrape
const url = "https://www.tvspielfilm.de/tv-programm/sendungen/?filter=1&order=time&freetv=1&cat%5B%5D=SP&time=day&channel=";

const TABLE_SELECTOR = 'table';

const startDate = new Date();


function sanitize(aNumber) {
    return (aNumber && aNumber >= 0 && aNumber <= 10) ? aNumber : 1;
}

// Async function which scrapes the data
async function scrapeData(offset, days) {
    try {
        let thedate = dtfns.addDays(startDate, sanitize(offset));
        let daycount = 0
        do {
            const theformatteddate = dtfns.format(thedate, 'yyyy-MM-dd');
            const urlwithdate = url + "&date=" + theformatteddate

            let page = 0;
            let weiter = 0;
            let inactive = 0;
            do {
                page++;
                // Fetch HTML of the page we want to scrape
                let { data } = await axios.get(urlwithdate + "&page=" + page);
                // Load HTML we fetched in the previous line
                let $ = await cheerio.load(data);

                let trs = $(TABLE_SELECTOR);

                // fs.writeFile("/data/tvspielfilm/tvspielfilm-" + theformatteddate + "_" + page + ".html", pretty(pretty(trs.html())), (err) => {
                //     if (err) {
                //         console.error(err);
                //         return;
                //     }
                //     console.log("Successfully written data to file");
                // });
                console.log(pretty(trs.html()));
                weiter = await $("#content > article > div.content-holder.normallisting > div > div > a.js-track-link.pagination__link.pagination__link--next")
                inactive = await $("#content > article > div.content-holder.normallisting > div > div > a.js-track-link.pagination__link.pagination__link--next.pagination__link--inactive");

            } while (weiter.length > 0 && inactive.length === 0)
            thedate = dtfns.addDays(thedate, 1);
            daycount++;
        } while (daycount < sanitize(days))

    } catch (err) {
        console.error(err);
    }
}

// Invoke the above function
// process.argv[]:
// 0: node
// 1: scriptname
// 2: offset, number of days from now, 0=today
// 3: days to scrape
scrapeData(process.argv[2], process.argv[3])
