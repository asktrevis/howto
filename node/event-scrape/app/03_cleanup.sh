#!/usr/bin/bash

if [ "$#" -ne 1 ]; then
  echo "missing venue"
  exit 1
fi

venue=$1

path="/data/${venue}"
if ! test -d "${path}"; then
  echo "${path} does not exist"
  exit 1
fi

findfile=`ls -1 ${path}/*txt`

i=0
for f in $findfile
do
  filelist[$i]="$f"
  i=$i+1
  #echo ${filelist[$i]}
done 

numberOfFiles=${#filelist[@]} 
if [ $numberOfFiles -eq 0 ]; then
  echo "no file found"
  exit 0
fi

 
filename="${filelist[$numberOfFiles-1]}"

awk -f suppressTVstations.awk ${filename} > ${filename}.full
awk -f suppressTVcrap.awk ${filename}.full > ${filename}.clean

cp ${filename}.clean ${path}/${venue}.clean 2> /dev/null
cp ${filename}.full ${path}/${venue}.full 2> /dev/null
