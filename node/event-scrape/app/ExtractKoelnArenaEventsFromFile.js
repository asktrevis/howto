const fs = require('fs');
const cheerio = require("cheerio");
const htmlparser2 = require('htmlparser2');

function decodeMonth(month) {
    switch (removeObsoleteChars(month)) {
        case 'Januar':
            return "01";
            break;
        case 'Februar':
            return "02";
            break;
        case 'März':
            return "03";
            break;
        case 'April':
            return "04";
            break;
        case 'Mai':
            return "05";
            break;
        case 'Juni':
            return "06";
            break;
        case 'Juli':
            return "07";
            break;
        case 'August':
            return "08";
            break;
        case 'September':
            return "09";
            break;
        case 'Oktober':
            return "10";
            break;
        case 'November':
            return "11";
            break;
        case 'Dezember':
            return "12";
            break;
        default:
            return null;
    }
}

function parseDate(strDay, strYear, strMonth) {
    return removeObsoleteChars(strYear) + "-" + decodeMonth(strMonth) + "-" + removeObsoleteChars(strDay);
}

function removeObsoleteChars(str) {
    return (str) ? str.replace(/(\r\n|\n|\r)/gm, ' ')
        .replace(/\s\s+/g, ' ')
        .trim() : null;
}

function scrapeData(filename) {
    try {
        // Fetch HTML of the page we want to scrape
        var document = fs.readFileSync(filename, 'utf8');
        const dom = htmlparser2.parseDocument(document);
        const $ = cheerio.load(dom);
        //console.log($.root().html());

        const SELECTOR = 'div.eventlist > div.eventlistitem';

        let divs = $(SELECTOR);
        // console.log($(SELECTOR).html());
        // console.log("number" + divs.length);
        const eventObj = {
            eventTitle: "", eventDate: ""
        };

        for (var i = 0; i < divs.length; i++) {
            let element = $(divs[i]);
            let tmpTitle = $('div.eventlistinfotitle', element).text();
            let tmpStatus = $('div.eventlistColStatus > a > div.eventstatus > span', element).text();
            if (tmpTitle) {
                eventObj.eventTitle = removeObsoleteChars(tmpStatus + " " + tmpTitle);
                // console.log("1x" + i + " " + eventObj.eventTitle);
                tmpTitle = undefined;
            }
            let tmpDtDay = $(element).attr('data-day');
            let tmpDtYear = $(element).attr('data-year');
            let tmpDtMonth = $(element).attr('data-month');
            //console.log("2x" + i + " " + tmpDtDay+ " " + tmpDtMonth+ " " + tmpDtYear);
            if (tmpDtDay && tmpDtYear && tmpDtMonth) {
                eventObj.eventDate = parseDate(tmpDtDay, tmpDtYear, tmpDtMonth);
                // console.log("2x" + i + " " + eventObj.eventDate);
                tmpDtTime = undefined;
                tmpDtYear = undefined;
                tmpDtMonth = undefined;
            }
            if (eventObj.eventDate && eventObj.eventTitle) {
                console.log(eventObj.eventDate + " " + eventObj.eventTitle);

                delete eventObj.eventDate;
                delete eventObj.eventTitle;
            }

        }

    } catch (err) {
        console.error(err);
    }
}

scrapeData(process.argv[2])


