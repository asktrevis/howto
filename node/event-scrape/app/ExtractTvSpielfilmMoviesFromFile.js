const fs = require('fs');
const cheerio = require("cheerio");
const htmlparser2 = require('htmlparser2');

function parseDate(str) {
    var m = str.match(/(\d{1,2})\.(\d{1,2})\.(\d{4})/);
    return (m) ? m[3] + "-" + m[2] + "-" + m[1] : null;
}

function removeObsoleteChars(str) {
    return (str) ? str
        .replace(/(\r\n|\n|\r)/gm, '')
        .replace(/\s\s+/g, ' ')
        .trim() : null;
}

function scrapeData(filename) {
    try {
        // Fetch HTML of the page we want to scrape
        var document = fs.readFileSync(filename, 'utf8');
        const dom = htmlparser2.parseDocument(document);
        const $ = cheerio.load(dom);
        //console.log($.root().html());

        const movies = [];

        const SELECTOR = 'tbody > tr.hover';

        let divs = $(SELECTOR);
        //console.log($(SELECTOR).html());
        //console.log("number" + divs.length);
        const movieObj = {
            movieTitle: "", movieDate: "", movieTime: "", movieStation: "", movieGenre: ""
            , movieType: "", movieRating: ""
        };

        for (var i = 0; i < divs.length; i++) {
            let element = $(divs[i]);

            let tmpStation = $('td.programm-col1 a', element).attr('title');
            if (tmpStation) {
                movieObj.movieStation = removeObsoleteChars(tmpStation);
                //console.log("1x" + i + " " + tmpStation);
                tmpStation = undefined;
            }
            let tmpDtTimeElement = $('td.col-2 div', element);
            if (tmpDtTimeElement) {
                let tmpDate = $('span', tmpDtTimeElement).text().trim();
                let tmpTime = $('strong', tmpDtTimeElement).text().trim();
                movieObj.movieDate = removeObsoleteChars(tmpDate);
                movieObj.movieTime = removeObsoleteChars(tmpTime);
                //console.log("2x" + i + " " + tmpDate + " " + tmpTime);
                tmpDtTimeElement = undefined;
            }
            let tmpTitle = $('td.col-3 > span > a', element).attr('title');
            if (tmpTitle) {
                movieObj.movieTitle = removeObsoleteChars(tmpTitle);
                //console.log("3x" + i + " " + tmpTitle);
                tmpTitle = undefined;
            }
            let tmpGenre = $('td.col-4 > span', element).text().trim();
            if (tmpGenre) {
                movieObj.movieGenre = removeObsoleteChars(tmpGenre);
                //console.log("4x" + i + " " + tmpGenre);
                tmpGenre = undefined;
            }
            let tmpType = $('td.col-5 > span', element).text().trim();
            if (tmpType) {
                movieObj.movieType = removeObsoleteChars(tmpType);
                //console.log("5x" + i + " " + tmpType);
                tmpType = undefined;
            }
            let tmpRating = $('td.col-6 > span', element).attr('class');
            if (tmpRating) {
                movieObj.movieRating = removeObsoleteChars(tmpRating);
                //console.log("6x" + i + " " + tmpRating);
                tmpRating = undefined;
            }
            if (movieObj.movieDate && movieObj.movieTime && movieObj.movieTitle && movieObj.movieStation && movieObj.movieGenre && movieObj.movieType) {
                console.log(movieObj.movieDate + " " + movieObj.movieTime
                    + " " + movieObj.movieTitle + " " + movieObj.movieStation
                    + " " + movieObj.movieGenre + " " + movieObj.movieType
                    + " " + movieObj.movieRating);

                delete movieObj.movieStation;
                delete movieObj.movieDate;
                delete movieObj.movieTime;
                delete movieObj.movieTitle;
                delete movieObj.movieGenre;
                delete movieObj.movieType;
                delete movieObj.movieRating;
            }

        }

    } catch (err) {
        console.error(err);
    }
}

scrapeData(process.argv[2])


