#!/usr/bin/bash

if [ "$#" -ne 1 ]; then
  echo "missing venue"
  exit 1
fi

venue=$1

path="/data/${venue}"
if ! test -d "${path}"; then
  echo "${path} does not exist"
  exit 1
fi

findfile=`ls -1 ${path}/*txt`

for f in $findfile
do
  filelist[$i]="$f"
  i=$i+1
  #echo ${filelist[$i]}
done 

numberoffiles=${#filelist[@]} 
if [ $numberoffiles -eq 0 ]; then
  echo "no file found"
  exit 0
fi

if [ $numberoffiles -eq 1 ]; then
  echo "special case" 
  exit 0
fi

rm ${path}/${venue}.${pid} 2> /dev/null

for (( c=$numberoffiles-1; c>0; c-- ))
do
 newerfilename="${filelist[$c]}"
 cutoffdate=`basename ${newerfilename} | cut -c 1-10`
 olderfilename="${filelist[$c-1]}"
 pid=$$
 rm ${newerfilename}.${pid} 2> /dev/null
 awk -v param=${cutoffdate} -f suppresslines.awk ${newerfilename} > ${newerfilename}.${pid}
 rm ${olderfilename}.${pid} 2> /dev/null
 awk -v param=${cutoffdate} -f suppresslines.awk ${olderfilename} > ${olderfilename}.${pid}
 
 command="diff ${olderfilename}.${pid} ${newerfilename}.${pid} >> ${path}/${venue}.${pid}"

 # echo "${command}";
 bash -c "${command}";
 rm ${newerfilename}.${pid}
 rm ${olderfilename}.${pid}
done

cp ${filelist[$numberoffiles-1]} ${path}/${venue}.full 2> /dev/null
mv ${path}/${venue}.${pid} ${path}/${venue}.diff 2> /dev/null

awk -f reorder-events.awk ${path}/${venue}.diff | sort -u > ${path}/${venue}.reordered
awk -f wrap_in_html.awk ${path}/${venue}.reordered > ${path}/${venue}.report
rm ${path}/${venue}.reordered 2> /dev/null
