const fs = require('fs');
const cheerio = require("cheerio");

function decodeMonth(month) {
    switch (removeObsoleteChars(month)) {
        case 'Jan':
            return "01";
            break;
        case 'Feb':
            return "02";
            break;
        case 'Mar':
            return "03";
            break;
        case 'Apr':
            return "04";
            break;
        case 'May':
            return "05";
            break;
        case 'June':
            return "06";
            break;
        case 'July':
            return "07";
            break;
        case 'Aug':
            return "08";
            break;
        case 'Sep':
            return "09";
            break;
        case 'Oct':
            return "10";
            break;
        case 'Nov':
            return "11";
            break;
        case 'Dec':
            return "12";
            break;
        default:
            return null;
    }
}

function pad(num, pattern) {
    var bufTarget = Buffer.from(pattern);
    Buffer.from(num).copy(bufTarget, pattern.length - num.length);
    return bufTarget.toString();
}

function parseDate(strDay, strYear, strMonth) {
    return removeObsoleteChars(strYear) + "-" + decodeMonth(strMonth) + "-" + removeObsoleteChars(pad(strDay, '00'));
}

function removeObsoleteChars(str) {
    return (str) ? str.replace(/(\r\n|\n|\r)/gm, ' ')
        .replace(/\s\s+/g, ' ')
        .trim() : null;
}

// Async function which scrapes the data
function scrapeData(filename) {
    try {
        // Fetch HTML of the page we want to scrape
        var data = fs.readFileSync(filename, 'utf8');
        const $ = cheerio.load(data);
        const events = [];

        const SELECTOR = '#content > div > div > div.eventList > div.list';

        let divs = $(SELECTOR).children();
        // console.log($(SELECTOR).html());
        // console.log("number" + divs.length);

        const eventObj = { eventDate: "", eventTitle: "" };

        for (var i = 0; i < divs.length; i++) {
            let element = $(divs[i]);

            let tmpTitle = $('div > h3 > a', element).text().trim();
            let tmpNote = $('div.event_note', element).text().trim();
            if (tmpTitle) {
                if (element.hasClass('cancelled')) {
                    eventObj.eventTitle = "Cancelled: " + " " + tmpTitle;
                }
                else {
                    eventObj.eventTitle = tmpTitle;
                }
                if (tmpNote) {
                    eventObj.eventTitle = removeObsoleteChars(eventObj.eventTitle + " (" + tmpNote + ")");
                }
                else {
                    eventObj.eventTitle = removeObsoleteChars(eventObj.eventTitle);
                }

                // console.log("1x" + i + " " + eventObj.eventTitle);
            }
            tmpTitle = undefined;
            tmpNote = undefined;
            tmpStatus = undefined;

            let tmpDtDay = $('div.date > span.m-date__singleDate > span.m-date__day', element).text().trim();
            let tmpDtYear = $('div.date > span.m-date__singleDate > span.m-date__year', element).text().trim();
            let tmpDtMonth = $('div.date > span.m-date__singleDate > span.m-date__month', element).text().trim();
            if (tmpDtDay && tmpDtYear && tmpDtMonth) {
                eventObj.eventDate = parseDate(tmpDtDay, tmpDtYear, tmpDtMonth);

                // console.log("2x" + i + " " + eventObj.eventDate);
            }
            else {
                let tmpDtRngFirstDay = $('div.date > span.m-date__rangeFirst > span.m-date__day', element).text().trim();
                let tmpDtRngFirstYear = $('div.date > span.m-date__rangeFirst > span.m-date__year', element).text().trim();
                let tmpDtRngFirstMonth = $('div.date > span.m-date__rangeFirst > span.m-date__month', element).text().trim();

                let tmpDtRngLastDay = $('div.date > span.m-date__rangeLast > span.m-date__day', element).text().trim();
                let tmpDtRngLastYear = $('div.date > span.m-date__rangeLast > span.m-date__year', element).text().trim();
                let tmpDtRngLastMonth = $('div.date > span.m-date__rangeLast > span.m-date__month', element).text().trim();

                if (tmpDtRngFirstMonth.length <= 1) { tmpDtRngFirstYear = tmpDtRngLastMonth }
                if (tmpDtRngFirstYear.length <= 1) { tmpDtRngFirstYear = tmpDtRngLastYear }


                eventObj.eventDate = parseDate(tmpDtRngFirstDay, tmpDtRngFirstYear, tmpDtRngFirstMonth) + " - " + parseDate(tmpDtRngLastDay, tmpDtRngLastYear, tmpDtRngLastMonth);

            }
            tmpDtTime = undefined;
            tmpDtYear = undefined;
            tmpDtMonth = undefined;

            if (eventObj.eventDate && eventObj.eventTitle) {
                console.log(eventObj.eventDate + " " + eventObj.eventTitle);

                delete eventObj.eventDate;
                delete eventObj.eventTitle;
            }
        }

    } catch (err) {
        console.error(err);
    }
}

scrapeData(process.argv[2])

