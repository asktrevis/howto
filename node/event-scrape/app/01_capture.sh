#!/usr/bin/bash
if [ "$#" -ne 1 ]; then
  echo "missing venue"
  exit 1
fi
venue=$1
file="01_${venue}_capture.sh"

date=$(date '+%Y-%m-%d_%H_%M_%S')
./${file}  > /data/${venue}/${date}_${venue}.html
