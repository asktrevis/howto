const axios = require("axios");
const pretty = require("pretty");

// URL of the page we want to scrape
const url = "https://ssl.webpack.de/termine.sporthallehamburg.de/pr/clipper.php";


// Async function which scrapes the data
async function scrapeData() {
  try {
    // Fetch HTML of the page we want to scrape
    const { data } = await axios.get(url);
      console.log(pretty(data));
  } catch (err) {
    console.error(err);
  }
}
// Invoke the above function
scrapeData();