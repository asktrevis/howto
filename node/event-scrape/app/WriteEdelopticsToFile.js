const axios = require("axios");
const pretty = require("pretty");

// URL of the page we want to scrape
const url = "https://edelopticsarena.de/event/";


// Async function which scrapes the data
async function scrapeData() {
  try {
    // Fetch HTML of the page we want to scrape
    const { data } = await axios.get(url);
      console.log(pretty(data));
  } catch (err) {
    console.error(err);
  }
}
// Invoke the above function
scrapeData();