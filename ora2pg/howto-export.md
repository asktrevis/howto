
There is a tool called export_schema.sh

It is created when project is initialized

export_schema does not export data, but gives a hint at the end of an export:

To extract data use the following command:

ora2pg -t COPY -o data.sql -b ./data -c ./config/ora2pg.conf
