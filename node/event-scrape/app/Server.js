var express = require('express');

const nrl = require('n-readlines');

var app = express();


// Async function which scrapes the data
async function readFile(filename) {
    var datalines = [];
    try {
        const lines = new nrl(filename);
        let line;
        while ((line = lines.next())) {
            datalines.push(line.toString('utf8'));
            // console.log(line.toString('ascii'));
        }

    } catch (err) {
        console.error(err);
    }

    return datalines;
}

app.get('/:target', function (req, res) {
    target = req.params.target.toString();
    console.log("Got a GET request for /" + target);
    switch (target) {
        case 'info':
            (async () => {
                let content = await readFile("/app/overview.html");
                res.send(content.join(""));
                return;
            })();
            break;
        case 'barclays':
        case 'sporthalle':
        case 'berlinmb':
        case 'berlinverti':
        case 'inselparkhalle':
        case 'edeloptics':
        case 'koelnarena':
        case 'london':
            (async () => {
                let events = await readFile("/data/" + target + "/" + target + ".diff");
                res.send(events.join("<br/>"));
            })();
            break;
        case 'tvspielfilm':
            (async () => {
                res.send(wrapInHtml(await readFile("/data/tvspielfilm/tvspielfilm.clean")));
            })();
            break;
        default:
            res.status(400).send();
            return;
    }
})

app.get('/:target/:mode', function (req, res) {
    target = req.params.target;
    mode = req.params.mode;
    console.log("Got a GET request for /" + target + "/" + mode);
    switch (mode) {
        case 'full':
        case 'report':
            break;
        default:
            res.status(400).send();
            return;
    }

    switch (target) {
        case 'barclays':
        case 'sporthalle':
        case 'berlinmb':
        case 'berlinverti':
        case 'inselparkhalle':
        case 'edeloptics':
        case 'koelnarena':
        case 'london':
            (async () => {
                let events = await readFile("/data/" + target + "/" + target + "." + mode);
                if (mode == "report") {
                    res.send(events.join(""));
                }
                else {
                    res.send(events.join("<br/>"));
                }
            })();
            break;
        case 'tvspielfilm':
            (async () => {
                res.send(wrapInHtml(await readFile("/data/tvspielfilm/tvspielfilm.full")));
            })();
            break;
        default:
            res.status(400).send();
            return;
    }
})


function wrapInHtml(movies) {
    let buf = Buffer.from("<!DOCTYPE html><body><style>table, th, td { border:1px solid black; } </style><table>");
    for (i = 0; i < movies.length; i++) {
        buf = Buffer.concat([buf, Buffer.from("<tr><td>" + movies[i] + "</td></tr>")]);
    }
    return Buffer.concat([buf, Buffer.from("</table></body></html>")]).toString();
}

var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("Listening at http://%s:%s", host, port)
})
