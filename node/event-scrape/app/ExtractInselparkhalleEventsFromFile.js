const fs = require('fs');
const cheerio = require("cheerio");
const htmlparser2 = require('htmlparser2');

function parseDate(str) {
    var m = str.match(/(\d{1,2})\.(\d{1,2})\.(\d{4})/);
    return (m) ? m[3] + "-" + m[2] + "-" + m[1] : null;
}

function removeObsoleteChars(str) {
    return (str) ? str.replace(/(\r\n|\n|\r)/gm, '')
    .replace(/\s\s+/g, ' ')
    .trim() : null;
}

function scrapeData(filename) {
    try {
        // Fetch HTML of the page we want to scrape
        var document = fs.readFileSync(filename, 'utf8');
        const dom = htmlparser2.parseDocument(document);
        const $ = cheerio.load(dom);
        //console.log($.root().html());

        const movies = [];

        const SELECTOR = 'div.vk-event';

        let divs = $(SELECTOR);
        //console.log($(SELECTOR).html());
        //console.log("number" + divs.length);
        const eventObj = {
            eventTitle: "", eventDate: ""
        };

        for (var i = 0; i < divs.length; i++) {
            let element = $(divs[i]);
            let tmpTitle = $('h3.vkal__teaser-title > a', element).text();
            if (tmpTitle) {
                eventObj.eventTitle = removeObsoleteChars(tmpTitle);
                //console.log("1x" + i + " " + tmpTitle);
                tmpTitle = undefined;
            }
            let tmpDtTime = $('p.hidden-xs-down', element).text();
            if (tmpDtTime) {
                eventObj.eventDate = parseDate(removeObsoleteChars(tmpDtTime));
                //console.log("2x" + i + " " + tmpDtTime);
                tmpDtTime = undefined;
            }
            if (eventObj.eventDate && eventObj.eventTitle) {
                console.log(eventObj.eventDate + " " + eventObj.eventTitle);

                delete eventObj.eventDate;
                delete eventObj.eventTitle;
            }

        }

    } catch (err) {
        console.error(err);
    }
}

scrapeData(process.argv[2])


