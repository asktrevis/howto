const datePart = new Date().toISOString().
  replace(/T/, '_').      // replace T with an underscore
  replace(/\..+/, '');