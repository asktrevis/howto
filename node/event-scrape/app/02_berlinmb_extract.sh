#!/usr/bin/bash
if [ "$#" -ne 1 ]; then
  echo "missing filepath"
  exit 1
fi

filepath=$1
file=`basename ${filepath} .html`
dir=`dirname ${filepath}`
targetpath="${dir}/${file}.txt"

node ExtractBerlinMbEventsFromFile.js ${filepath} > ${targetpath} 
