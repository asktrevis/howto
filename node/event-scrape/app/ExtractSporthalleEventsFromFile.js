const fs = require('fs');
const cheerio = require("cheerio");

function parseDate(str) {
    var m = str.match(/(\d{1,2})\.(\d{1,2})\.(\d{4})/);
    return (m) ? m[3] + "-" + m[2] + "-" + m[1] : null;
}

function scrapeData(filename) {
    try {
        // Fetch HTML of the page we want to scrape
        var data = fs.readFileSync(filename, 'utf8');
        const $ = cheerio.load(data);

        const divs = $("div")

        const eventObj = { eventDate: "", eventTitle: "" };
        divs.each(function (idx, el) {
            blockEvaluation = false;
            if (!eventObj.eventTitle) {
                tmpTitle = $(el).children("h3").text().trim();
                if (tmpTitle) {
                    eventObj.eventTitle = tmpTitle;
                    tmpTitle = "";
                    tmpDate = "";
                    blockEvaluation = true;
                }
            }
            if (!blockEvaluation && eventObj.eventTitle && !eventObj.eventDate) {
                tmpDate = $(el).text().trim();
                if (tmpDate) {
                    eventObj.eventDate = parseDate(tmpDate);
                    tmpTitle = "";
                    tmpDate = "";
                    blockEvaluation = false;
                }
            }
            if (eventObj.eventDate && eventObj.eventTitle) {
                console.log(eventObj.eventDate + " " + eventObj.eventTitle);
                delete eventObj.eventDate;
                delete eventObj.eventTitle;
            }
        });


    } catch (err) {
        console.error(err);
    }
}

scrapeData(process.argv[2])

