tar cvf es.tar app/0*\
        app/Extract*\
        app/install_prerequisites.sh\
        app/Server.js\
        app/suppress*.awk\
        app/Write*\
        app/set-x-bit.sh\
        app/overview.html\
        enter_shell.sh\
        cronfile\
        exec0*\
        targetsystem\
        set_owner_data.sh\
        create_tar.sh

