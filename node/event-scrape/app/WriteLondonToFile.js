const playwright = require('playwright');
const pretty = require('pretty');

// URL of the page we want to scrape
const url = "https://www.theo2.co.uk/events/venue/the-o2-arena";

(async () => {
    const browser = await playwright.chromium.launch();
    const page = await browser.newPage();

    //    const LOAD_MORE_SELECTOR = '.loadMoreEvents';
    const LOAD_MORE_SELECTOR = '(//*[@id="loadMoreEvents"])';
    const CLOSE_COOKIE_CONSENT_SELECTOR = '(//*[@id="onetrust-close-btn-container"]/button)';

    await page.goto(url);
    await page.waitForTimeout(1000);

    // cookie consent
    await page.waitForSelector(CLOSE_COOKIE_CONSENT_SELECTOR);
    await page.click(CLOSE_COOKIE_CONSENT_SELECTOR);

    // wait for the page with the event data
    await page.waitForSelector(LOAD_MORE_SELECTOR);

    while (await page.locator(LOAD_MORE_SELECTOR).isVisible()) {
        await page.waitForSelector(LOAD_MORE_SELECTOR);
        await page.click(LOAD_MORE_SELECTOR);
        await page.waitForTimeout(2000);
    }
    console.log(pretty(await page.content()));
    //await page.screenshot({path: 'example.png'});

    await browser.close();
})();
