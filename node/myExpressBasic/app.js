var express = require('express');

const axios = require("axios");
const cheerio = require("cheerio");

// URL of the page we want to scrape
const url = "https://ssl.webpack.de/termine.sporthallehamburg.de/pr/clipper.php";

var app = express();


// Async function which scrapes the data
async function scrapeData() {
   var events = [];
   try {
      // Fetch HTML of the page we want to scrape
      const { data } = await axios.get(url);
      // Load HTML we fetched in the previous line
      const $ = await cheerio.load(data);
      const cp = await $("cp_termine")

      const divs = await $("div")

      const eventObj = { eventDate: "", eventTitle: "" };
      divs.each(function (idx, el) {
         blockEvaluation = false;
         if (!eventObj.eventTitle) {
            tmpTitle = $(el).children("h3").text().trim();
            if (tmpTitle) {
               eventObj.eventTitle = tmpTitle;
               tmpTitle = "";
               tmpDate = "";
               blockEvaluation = true;
            }
         }
         if (!blockEvaluation && eventObj.eventTitle && !eventObj.eventDate) {
            tmpDate = $(el).text().trim();
            if (tmpDate) {
               eventObj.eventDate = tmpDate;
               tmpTitle = "";
               tmpDate = "";
               blockEvaluation = false;
            }
         }
         if (eventObj.eventDate && eventObj.eventTitle) {
            events.push(eventObj.eventDate + " " + eventObj.eventTitle)
            //console.log(eventObj.eventDate + " " + eventObj.eventTitle);

            delete eventObj.eventDate;
            delete eventObj.eventTitle;
         }
      });

   } catch (err) {
      console.error(err);
   }
   
   return events;
}



// Invoke the above function
// This responds a GET request for the /list_user page.
app.get('/sporthalle', function (req, res) {
   console.log("Got a GET request for /sporthalle");
   (async () => {
      let events = await scrapeData();
      res.send(events.join("<br/>"));
      //console.log("Rausgesendet");
   })();
   
})


var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port

   console.log("Example app listening at http://%s:%s", host, port)
})
