const axios = require("axios");
const cheerio = require("cheerio");
const fs = require("fs");
const dtfns = require('date-fns');
const pretty = require("pretty");

// URL of the page we want to scrape
const starturl = "https://www.hamburg.de/tickets/l/225168/inselparkhalle-wilhelmsburg-hamburg.html";

const TABLE_SELECTOR = '#vk-event-wrapper';

// Async function which scrapes the data
async function scrapeData() {
    try {
        let url = starturl;
        do {
            // Fetch HTML of the page we want to scrape
            let { data } = await axios.get(url);
            // console.log(pretty(data));
            // Load HTML we fetched in the previous line
            let $ = await cheerio.load(data);

            let divs = $(TABLE_SELECTOR);

            // fs.writeFile("/data/tvspielfilm/tvspielfilm-" + theformatteddate + "_" + page + ".html", pretty(pretty(trs.html())), (err) => {
            //     if (err) {
            //         console.error(err);
            //         return;
            //     }
            //     console.log("Successfully written data to file");
            // });

            console.log(pretty(divs.html()));

            let next = "div.result-list-pagination > div.container-buttons > div.result-list-pagination > ul > li > a.next-page";
            url = $(next).attr('href');

        } while (url && url.length > 0)

    } catch (err) {
        console.error(err);
    }
}

// Invoke the above function
scrapeData()
