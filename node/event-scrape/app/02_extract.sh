#!/usr/bin/bash
if [ "$#" -ne 1 ]; then
  echo "missing venue"
  exit 1
fi
venue=$1
filetoexecute="02_${venue}_extract.sh"

filelist=`find /data/${venue}/ -name "*html"`

for file in ${filelist} 
do
    filebasename=`basename ${file} .html`
    dir=`dirname ${file}`
    pathtosearch="${dir}/${filebasename}.txt"

    #echo ${pathtosearch};
    if ! test -f "${pathtosearch}"; then
    execstring="./${filetoexecute} ${file}"
    echo "execute ${execstring}"
    $execstring 
    fi
done
