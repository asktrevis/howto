const fs = require('fs');
const cheerio = require("cheerio");

function removeObsoleteChars(str) {
    return (str) ? str.replace(/(\r\n|\n|\r)/gm, ' ')
        .replace(/\s\s+/g, ' ')
        .trim() : null;
}

// Async function which scrapes the data
function scrapeData(filename) {
    try {
        // Fetch HTML of the page we want to scrape
        var data = fs.readFileSync(filename, 'utf8');
        const $ = cheerio.load(data);
        const events = [];

        const SELECTOR = '#list';
        let divs = $(SELECTOR).children();
        // console.log($(SELECTOR).html());
        // console.log("number" + divs.length);

        const eventObj = { eventDate: "", eventTitle: "" };

        for (var i = 0; i < divs.length; i++) {
            let element = $(divs[i]);

            let tmpTitle = $('div > h3 > span', element).text().trim();
            let tmpNote = $('div.event_note', element).text().trim();
            if (tmpTitle) {
                if (element.hasClass('cancelled')) {
                    eventObj.eventTitle = "Cancelled: " + " " + tmpTitle;
                }
                else {
                    eventObj.eventTitle = tmpTitle;
                }
                if (tmpNote) {
                    eventObj.eventTitle = removeObsoleteChars(eventObj.eventTitle + " (" + tmpNote + ")");
                }
                else {
                    eventObj.eventTitle = removeObsoleteChars(eventObj.eventTitle);
                }

                // console.log("1x" + i + " " + eventObj.eventTitle);
                tmpTitle = undefined;
                tmpNote = undefined;
                tmpStatus = undefined;
            }

            let tmpDtDay = $('div.date > span > span.m-date__day', element).text();
            let tmpDtYear = $('div.date > span > span.m-date__year', element).text();
            let tmpDtMonth = $('div.date > span > span.m-date__month', element).text();
            if (tmpDtDay && tmpDtYear && tmpDtMonth) {
                eventObj.eventDate = (tmpDtYear + "-" + tmpDtMonth + "-" + tmpDtDay)
                    .replaceAll(/\./ig, '');;
                // console.log("2x" + i + " " + eventObj.eventDate);
                tmpDtTime = undefined;
                tmpDtYear = undefined;
                tmpDtMonth = undefined;
            }

            if (eventObj.eventDate && eventObj.eventTitle) {
                console.log(eventObj.eventDate + " " + eventObj.eventTitle);

                delete eventObj.eventDate;
                delete eventObj.eventTitle;
            }
        }

    } catch (err) {
        console.error(err);
    }
}

scrapeData(process.argv[2])

